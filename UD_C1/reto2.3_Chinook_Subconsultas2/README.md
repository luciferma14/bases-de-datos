# Reto 2.2: Consultas con subconsultas II
Lucía Ferrandis Martínez.

[Bases de Datos] Unidad Didáctica 2: DML - Subconsultas II
Para este reto, usaremos la base de datos Chinook, que podemos encontrar en el siguiente repositorio: https://github.com/lerocha/chinook-database/ . La base de datos Chinook simula una tienda de música en línea e incluye tablas para artistas, álbumes, canciones, clientes, empleados, géneros musicales, pedidos y facturas. Cada tabla contiene información relevante para su entidad respectiva, como nombres de artistas, títulos de álbumes,géneros musicales, detalles de pedidos y facturas, entre otros. Esta estructura permite practicar consultas SQL relacionadas con la gestión de una  tienda de música, desde la administración de inventario hasta el análisis de ventas y clientes.


* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto2.3_Chinook?ref_type=heads)

# Query 1
En este ejercicio nos pide que mostremos las canciones con una duración superior a la media. Para esto, hago un `SELECT` de los IDs de las canciones y de los milisegundos de estas y, luego realizo unas subconsulta sobre los milisegundos, que seleccione la media de estos. Y le pongo `>` para que me muestre los milisegundos mayores a la media de ellos.

```sql
SELECT TrackId, Milliseconds
FROM Track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);
```

# Query 2
En este ejercicio nos pide que mostremos las 5 últimas facturas del cliente cuyo email es “emma_jones@hotmail.com”. Para esto, hago un `SELECT` de los IDs de las facturas, luego realizo una subconsulta sobre los IDs de los clientes, que seleccione estos, donde el email sea igual que "emma_jones@hotmail.com". Y después ordeno por la fecha de las facturas de manera descendente.

```sql
SELECT InvoiceId
FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId
    FROM Customer
    WHERE Email LIKE "emma_jones@hotmail.com"
)
ORDER BY InvoiceDate DESC
LIMIT 5;
```

# Query 3
En este ejercicio nos pide que mostremos las listas de reproducción en las que hay canciones de reggae. Para esto, hago un `SELECT` de los IDs de las playlists y de sus nombres, luego realizo unas subconsulta sobre los IDs de las playlists, que seleccione estos sobre la tabla de `PlaylistTrack`, y sobre esta tabla, hago otra subconsulta, que seleciono de los IDs de las canciones, los IDs de los géneros, donde el ID sea igual que 8, que es ID del Reggae.

```sql
SELECT PlaylistId, Name 
FROM Playlist
WHERE PlaylistId IN (
	SELECT PlaylistId
	FROM PlaylistTrack
	WHERE TrackId IN (
		SELECT GenreId 
		FROM Genre
		WHERE GenreId LIKE "8"
	)
);
```

# Query 4
En este ejercicio nos pide que mostremos la información de los clientes que han realizado compras superiores a 20€. Para esto, hago un `SELECT` de los IDs de los clientes y de sus nombres, también los IDs y la fecha de las facturas. Luego realizo un `JOIN` sobre la tabla de las facturas, igualando los IDs de los clientes. Luego hago una subconsulta que realiza la suma de la multiplicación entre el precio unitario y la cantidad sobre la tabla de las líneas de las facturas, e igualo los IDs de las facturas, para sacar el precio; y solo muestro las que superen los 20€. Luego, agrupo por lo mismo que he seleccionado al principio y ordeno por los IDs de los clientes de manera ascendente.

```sql
SELECT C.CustomerId, C.FirstName, C.LastName, I.InvoiceId, I.InvoiceDate
FROM customer C
JOIN invoice I 
ON C.CustomerId = I.CustomerId
WHERE (
  SELECT SUM(UnitPrice * Quantity)
  FROM invoiceline IL
  WHERE IL.InvoiceId = I.InvoiceId
) > 20
GROUP BY C.CustomerId, C.FirstName, C.LastName, I.InvoiceId, I.InvoiceDate
ORDER BY C.CustomerId ASC;
```

# Query 5
En este ejercicio nos pide que mostremos los álbumes que tienen más de 15 canciones, junto a su artista. Para esto, hago un `SELECT` de todos los datos de la tabla de los álbumes. Luego realizo un `JOIN` sobre la tabla de los artistas, usando `USING` igualo los IDs de los artistas. Luego hago una subconsulta sobre los IDs de los álbumes de la tabla de las canciones, y lo agrupo por los IDs de los álbumes. Y con el `HAVING` digo que solo muestre los que tengas más de 15 canciones.

```sql
SELECT * 
FROM Album 
JOIN Artist USING (ArtistId)
WHERE AlbumId IN (
	SELECT AlbumId 
	FROM Track
	GROUP BY AlbumId
	HAVING COUNT(*) > 15
);
```

# Query 6
En este ejercicio nos pide que mostremos los álbumes con un número de canciones superiores a la media. Para esto, hago un `SELECT` del los IDs de los álbumes y cuento todo de la tabla de las canciones, agrupándolo por los IDs de los álbumes, llamándolo "N_Canciones". Luego con el `HAVING` digo que solo muestre los "N_Canciones" mayores que la media de estos; haciendo una subconsulta, para conseguir la tabla llamada "Album_NCanciones".

```sql
SELECT AlbumId, COUNT(*) AS N_Canciones
FROM Track
GROUP BY AlbumId
HAVING N_Canciones > (
	SELECT AVG(N_Canciones)
	FROM (
		SELECT AlbumId, COUNT(*) AS N_Canciones
		FROM Track
		GROUP BY AlbumId
	) AS Album_NCanciones
);
```

# Query 7
En este ejercicio nos pide que mostremos los álbumes con una duración total superior a la media. Para esto, hago un `SELECT` del los IDs y nombres de los álbumes y sumo todos los milisegundos de la tabla de las canciones, llamándolo "TotalDuracion". También hago una subconsulta para sacar la media de los milisegundos, llamándolo "MediaDuracion". Después hago un `JOIN` con la tabla de las canciones, usandos los IDs de los álbumes y agrupando por estos y por el titilo. Luego con el `HAVING` digo que solo muestre los que tengan una duración total mayor que la de la media.

```sql
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS TotalDuracion,
  ( SELECT AVG(T2.Milliseconds)
    FROM track T2
  ) AS MediaDuracion
FROM album A
JOIN track T USING(AlbumId)
GROUP BY A.AlbumId, A.Title
HAVING TotalDuraCion > MediaDuracion;
```

# Query 8
En este ejercicio nos pide que mostremos las canciones del género con más canciones. Para esto, hago un `SELECT` del los IDs de los géneros y cuento todos los IDs de estos, llamándolos "N_Canciones". Luego hago un `JOIN` con la tabla de las canciones, usando los IDs de los géneros. Después agrupo por los IDs de estos y por último, limito a 1 resultado.

```sql
SELECT GenreId, COUNT(*) AS N_Canciones
FROM genre
JOIN track USING(GenreId)
GROUP BY GenreId
LIMIT 1;
```

# Query 9
En este ejercicio nos pide que mostremos las canciones s de la playlist con más canciones. Para esto, hago un `SELECT` del los IDs de las playlist y cuento todos los IDs de estas, llamándolos "N_Canciones". Luego hago un `JOIN` con la tabla de playlistTrack, usando los IDs de las playlist. Después agrupo por los IDs de estas y por último, limito a 1 resultado.

```sql
SELECT PlaylistId, COUNT(*) AS N_Canciones
FROM playlist
JOIN playlisttrack USING(PlaylistId)
GROUP BY PlaylistId
LIMIT 1;
```

# Query 10
En este ejercicio nos pide que mostremos los clientes junto con la cantidad total de dinero gastado por cada uno en compras. Para esto, hago un `SELECT` del los IDs, nombres y apellidos de los clientes y hago unas subconsulta para sacar el total de los gastos de cada cliente, llamándolo "Total_Gastado". Y por último, ordeno sobre el total gastado de manera descendente.

```sql
SELECT C.CustomerId, C.FirstName, C.LastName, (
    SELECT SUM(UnitPrice * Quantity)
    FROM invoiceline IL
    WHERE IL.InvoiceId IN (
      SELECT InvoiceId
      FROM invoice I
      WHERE I.CustomerId = C.CustomerId
    )
  ) AS Total_Gastado
FROM customer C
ORDER BY Total_Gastado DESC;
```

# Query 11
En este ejercicio nos pide que mostremos los empleados y el número de clientes a los que sirve cada uno de ellos. Para esto, hago un `SELECT` del los IDs, nombres y apellidos de los empleados y hago unas subconsulta para sacar el total de número de clientes que tiene cada uno, igualando los IDs respectivamente, llamándolo "N_Customers".

```sql
SELECT EmployeeId, FirstName, LastName, (
	SELECT COUNT(*) AS N_Customers
    FROM Customer C
    WHERE C.SupportRepId = E.EmployeeId
    ) AS N_Customers
FROM Employee E;
```

# Query 12
En este ejercicio nos pide que mostremos las ventas totales de cada empleado. Para esto, hago un `SELECT` del los IDs, nombres y apellidos de los empleados y hago unas subconsulta para sacar el total de número de ventas que tiene cada uno, intercalando las tablas de facturas y la de las líneas de estas, llamándola "Total_ventas". Y por último, ordeno por este dato de manera descendente.

```sql
SELECT E.EmployeeID, E.FirstName, E.LastName, 
(
	SELECT SUM(UnitPrice * Quantity)
	FROM invoiceline IL
	WHERE IL.InvoiceId IN (
	  SELECT InvoiceId
	  FROM invoice I
	  WHERE I.CustomerID IN (
		SELECT CustomerID
		FROM customer C
		WHERE C.SupportRepId = E.EmployeeID
	  )
	)
) AS Total_ventas
FROM employee E
ORDER BY Total_ventas DESC;
```

# Query 13
En este ejercicio nos pide que mostremos los álbumes junto al número de canciones en cada uno. Para esto, hago un `SELECT` del los IDs, títulos de los álbumes y hago unas subconsulta para sacar el total de número de canciones que tiene cada uno, llamándola "N_Canciones".

```sql
SELECT A.AlbumId, A.Title,
  ( SELECT COUNT(*)
    FROM track T
    WHERE T.AlbumId = A.AlbumId
  ) AS N_Canciones
FROM album A;
```

# Query 14
En este ejercicio nos pide que mostremos el nombre del álbum más reciente de cada artista. Para esto, hago un `SELECT` del nombre de los artitas y del título de los álbumes. Luego hago un `JOIN` sobre la tabla de los álbumes, usando los IDs de los artistas. Después realizo una subconsulta para calcular los IDs máximos, por que son los más nuevos, igualando los IDs de los artistas respectivamente. Y por último, ordeno por los IDs de los artistas de manera ascendente.

```sql
SELECT AR.Name AS Nombre_Artista, AL.Title AS Ultimo_Album
FROM artist AR
JOIN album AL USING(ArtistId)
WHERE AL.AlbumId = (
  SELECT MAX(AL2.AlbumId)
  FROM album AL2
  WHERE AL2.ArtistId = AR.ArtistId
)
ORDER BY AR.ArtistId ASC;
```