SELECT A.AlbumId, A.Title,
  ( SELECT COUNT(*)
    FROM track T
    WHERE T.AlbumId = A.AlbumId
  ) AS N_Canciones
FROM album A;