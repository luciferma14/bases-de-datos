SELECT SupportRepId, COUNT(*) AS N_Customers
FROM Customer
GROUP BY SupportRepId;
    
-- Sacar info de cada empleado (usando JOIN)
SELECT *
FROM Employee
JOIN (
	SELECT SupportRepId, COUNT(*) AS N_Customers
	FROM Customer
	GROUP BY SupportRepId
) AS Empleado_NCustomers;

-- Sacar ino de cada empleado (usando subconsultas correlacionadas)
SELECT EmployeeId, FirstName, LastName, (
	SELECT COUNT(*) AS N_Customers
    FROM Customer
    WHERE Customer.SupportRepId = Employee.EmployeeId
    ) AS N_Customers
FROM Employee;