SELECT E.EmployeeID, E.FirstName, E.LastName, 
(
	  SELECT SUM(Total)
	  FROM Invoice I
	  WHERE I.CustomerID IN (
		SELECT CustomerID
		FROM Customer C
		WHERE C.SupportRepId = E.EmployeeID
	  )
) AS Total_ventas
FROM Employee E
ORDER BY Total_ventas DESC;