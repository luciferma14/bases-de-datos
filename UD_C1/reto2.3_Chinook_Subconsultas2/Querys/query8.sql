SELECT GenreId, COUNT(*) AS N_Canciones
FROM genre
JOIN track USING(GenreId)
GROUP BY GenreId
LIMIT 1;