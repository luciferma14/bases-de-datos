SELECT PlaylistId, Name 
FROM Playlist
WHERE PlaylistId IN (
	SELECT PlaylistId
	FROM PlaylistTrack
	WHERE TrackId IN (
		SELECT GenreId 
		FROM Genre
		WHERE GenreId LIKE "8"
	)
);