SELECT C.CustomerId, C.FirstName, C.LastName, (
    SELECT SUM(UnitPrice * Quantity)
    FROM invoiceline IL
    WHERE IL.InvoiceId IN (
      SELECT InvoiceId
      FROM invoice I
      WHERE I.CustomerId = C.CustomerId
    )
  ) AS Total_Gastado
FROM customer C
ORDER BY Total_Gastado DESC;