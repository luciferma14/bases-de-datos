SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS TotalDuracion,
  ( SELECT AVG(T2.Milliseconds)
    FROM track T2
  ) AS MediaDuracion
FROM album A
JOIN track T USING(AlbumId)
GROUP BY A.AlbumId, A.Title
HAVING TotalDuraCion > MediaDuracion;