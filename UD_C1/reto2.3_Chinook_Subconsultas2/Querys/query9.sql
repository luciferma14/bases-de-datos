SELECT PlaylistId, COUNT(*) AS N_Canciones
FROM playlist
JOIN playlisttrack USING(PlaylistId)
GROUP BY PlaylistId
LIMIT 1;