SELECT AlbumId, COUNT(*) AS N_Canciones
FROM Track
GROUP BY AlbumId
HAVING N_Canciones > (
	SELECT AVG(N_Canciones)
	FROM (
		SELECT AlbumId, COUNT(*) AS N_Canciones
		FROM Track
		GROUP BY AlbumId
	) AS Album_NCanciones
);