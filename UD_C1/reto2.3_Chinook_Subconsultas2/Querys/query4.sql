SELECT C.CustomerId, C.FirstName, C.LastName, I.InvoiceId, I.InvoiceDate
FROM customer C
JOIN invoice I 
ON C.CustomerId = I.CustomerId
WHERE (
  SELECT SUM(UnitPrice * Quantity)
  FROM invoiceline IL
  WHERE IL.InvoiceId = I.InvoiceId
) > 20
GROUP BY C.CustomerId, C.FirstName, C.LastName, I.InvoiceId, I.InvoiceDate
ORDER BY C.CustomerId ASC;