SELECT AR.Name AS Nombre_Artista, AL.Title AS Ultimo_Album
FROM artist AR
JOIN album AL USING(ArtistId)
WHERE AL.AlbumId = (
  SELECT MAX(AL2.AlbumId)
  FROM album AL2
  WHERE AL2.ArtistId = AR.ArtistId
)
ORDER BY AR.ArtistId ASC;