# Reto 2: Consultas básicas II
Lucía Ferrandis Martínez.

En este reto trabajamos con la base de datos `empresa`, que nos viene dada en el fichero `empresa.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto2_empresa?ref_type=heads)

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/

## Query 1
En este primer ejercicio nos pide que mostremos el código y la descripción de los productos que se comercializan en la empresa.
Para esto, realizo un `SELECT` de la descripción (`DESCRIPCIO`) y le pongo el nombre Descripción; y también hago un `SELECT` del código, que en la tabla es `PROD_NUM`. Todos estos datos los saco de la tabla `PRODUCTE` haciendo un `FROM`. El código quedaría así:

```sql
SELECT 
	DESCRIPCIO AS 'Descripción',
	PROD_NUM AS "Código"
FROM PRODUCTE;
```

## Query 2
Este ejercicio es muy parecido al anterior, lo único diferente es que, en este nos pedia ademas de lo anterior, que añadieramos los productos que contienen la palabra tenis en la descripción. 
Esto lo realizamos añadiendo debajo del `FROM` un `WHERE` donde decimos que el contenido de la columna `DESCRIPCIO` sea igual `TENNIS`, añadiendo los % para decir que coja todos los carateres restantes para llegar hasta tenis. El código quedaría así:

```sql
SELECT 
	DESCRIPCIO AS 'Descripción',
	PROD_NUM AS "Código"
FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';
```

## Query 3
En este ejercicio nos pide que mostremos el código, nombre, área y teléfono de los clientes de la empresa.
Para hacer esto, hacemos un `SELECT` del código del cliente (`CLIENTE_COD`), del nombre y teléfono del éste (`NOM`, `TELEFON`) y del área (`AREA`). Todo esto lo sacamos de la tabla `CLIENT` haciendo un `FROM`. El código quedaría así:

```sql
SELECT 
	CLIENT_COD AS 'Código cliente',
    NOM AS "Nombre",
    TELEFON AS Teléfono
    AREA AS 'Área',
FROM CLIENT;
```

## Query 4
En este ejercicio nos pide que mostremos el código, nombre y ciudad de los clientes de la empresa que no sean del área telefónica 636.
Para realizar esto, hago un `SELECT` de estos datos respectivamente (`CLIENT_COD`, `NOM`, `CIUTAT`), y los sacamos de la tabla `CLIENT` haciendo un `FROM`. Y para sacar los que no sean del área telefónica 636, hacemos de la `WHERE` donde decimos que la columna `TELEFON` no sea igual (`NOT LIKE`) a los números 636. El código nos quedaría así:

```sql
SELECT 
	CLIENT_COD AS 'Código cliente',
	NOM AS "Nombre",
    CIUTAT AS Ciudad
FROM CLIENT
WHERE AREA NOT LIKE "%636%";
```

## Query 5
En este ejercicio nos pide que mostremos las órdenes de compra; que serían el código, y las fechas de orden y de envio, de los productos.
Para realizar esto, lo que hacemos es un `SELECT` de todos los datos nombrados anteriormente; que serían: `COM_NUM`, `COM_DATA`, `DATA_TRASMETA` respectivamente. Y todos estos datos los sacamos de la tabla `COMANDA` haciendo un `FROM`. El código quedaría así:

```sql
SELECT 
	COM_NUM AS 'Código',
	COM_DATA AS "Fecha de orden",
    DATA_TRAMESA AS "Fecha de envio"
FROM COMANDA;
```