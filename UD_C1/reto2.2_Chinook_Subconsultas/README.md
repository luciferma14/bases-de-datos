# Reto 2.2: Consultas con subconsultas
Lucía Ferrandis Martínez.

[Bases de Datos] Unidad Didáctica 2: DML - Subconsultas
Para este reto, usaremos la base de datos Chinook, que podemos encontrar en el siguiente repositorio: https://github.com/lerocha/chinook-database/ . La base de datos Chinook simula una tienda de música en línea e incluye tablas para artistas, álbumes, canciones, clientes, empleados, géneros musicales, pedidos y facturas. Cada tabla contiene información relevante para su entidad respectiva, como nombres de artistas, títulos de álbumes,géneros musicales, detalles de pedidos y facturas, entre otros. Esta estructura permite practicar consultas SQL relacionadas con la gestión de una  tienda de música, desde la administración de inventario hasta el análisis de ventas y clientes.


* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto2.2_Chinook_Subconsultas?ref_type=heads)

# Query 1
En este primer ejercicio nos pide que mostremos las listas de reproducción cuyo nombre comienza por M, junto a las 3 primeras canciones de cada uno, ordenadas por álbum y por precio (más bajo primero). Para empezar hago un `SELECT DISTINCT` para selecionar el nombre de la lista de reproducción, que asegura que no aparezcan datos duplicados, y el nombre de la canción. En el `WHERE` digo que solo me saquen las playlist que empiezen por la letra "M". El `EXISTS`, comprueba si una subconsulta devuelve al menos 1 fila; y el `SELECT 1` simplemente selecciona el valor 1, para recuperar datos reales. Después hago un `JOIN` con las tablas `Track` y `Album` e igualo sus álbumes IDs. Luego hago una subconsulta sobre el precio unitario de las canciones, y selecciono el mínimo precio de estas; y después vuelvo a hacer un `JOIN` igualando los álbumes IDs de las tablas `Track` y `Album`, los IDs de las canciones de las tablas `Track` y `playlisttrack` y también los ID de los álbumes de las tablas `Track` y `Album`, pero esta vez del album del principio. Y por último, limito todos los datos a tres.

```sql
SELECT DISTINCT(P.Name), T.Name
FROM playlist P, track T
WHERE P.Name LIKE 'M%'
AND EXISTS (
    SELECT 1
    FROM track T, playlisttrack PT
    JOIN Album A 
    ON T.AlbumId = A.AlbumId
    WHERE T.TrackId = PT.TrackId
    AND T.UnitPrice = (
        SELECT MIN(UnitPrice)
        FROM track T2
        JOIN Album A2 
        ON T2.AlbumId = A2.AlbumId
        WHERE T2.TrackId = PT.TrackId
        AND T2.AlbumId = A.AlbumId
        LIMIT 3
    )
    LIMIT 3
);
```

# Query 2
En este ejercicio nos pide que mostremos todos los artistas que tienen canciones con duración superior a 5 minutos.
Para hacer esto, lo que hago es hacer un `SELECT` de el ID de los artistas y de sus nombres. Donde hago una subconsulta seleccionando el ID de los artistas de la tabla de los álbumes y haciendo otra subconsulta sobre los IDs de los álbumes pero esta vez sobre la tabla de las canciones. Y después hago otra subconsulta sobre los milisegundos de las canciones y digo que me muestre solo los que superen los 300000 segundos.

```sql
SELECT A.ArtistId, A.Name
FROM artist A
WHERE ArtistId IN (
	SELECT ArtistId
    FROM album
    WHERE AlbumId IN (
		SELECT AlbumId
        FROM track T
        WHERE Milliseconds IN (
			SELECT Milliseconds
            FROM track
            WHERE Milliseconds > 300000
        )
    )
);
```

# Query 3
En este ejercicio nos pide que mostremos los nombre y apellidos de los empleados que tienen clientes asignados. Para esto, hago un `SELECT` de los dichos nombres y apellidos y, utlizando el `EXISTS` veo si existe la relación entre los empleados y sus supervisores. 

```sql
SELECT FirstName, LastName
FROM Employee
WHERE EXISTS (
	SELECT * -- true, 1, ... 
    FROM Customer
    WHERE SupportRepId = EmployeeId
);
```

# Query 4
En este ejercicio nos pide que mostremos todas las canciones que no han sido compradas. Para esto, hago un `SELECT` del ID de las canciones, los nombres y los compositores y, luego realizo una subconsulta sobre los ID de las canciones que no están en la facutura.

```sql
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId
    FROM InvoiceLine
);
```

# Query 5
En este ejercicio nos pide que mostremos los empleados junto a sus subordinados (empleados que reportan a ellos). Para esto, hago un `SELECT` del nombre de los empleados y, luego realizo una subconsulta sobre los que reportan a los empleados y hago un `JOIN` de la misma tabla, igualando los IDs de los empleados con sus represnetantes. Y para acabar, los ordeno por los nombre.

```sql
SELECT E.FirstName, (
	SELECT E.ReportsTo
    FROM employee E1
    JOIN employee E2
    ON E1.EmployeeId = E2.ReportsTo
    WHERE E.ReportsTo = E1.EmployeeId
)
FROM employee E
ORDER BY E.FirstName;
```

# Query 6
En este ejercicio nos pide que mostremos todas las canciones que ha comprado el cliente Luis Roja. Para esto, hago un `SELECT` del ID, los nombre y los compositores de las canciones,  y, luego realizo unas subconsulta sobre los IDs de las canciones sobre la tabla de las líneas de las facturas; hago otra, que recoje los IDs de las facturas sobre la tabla de las facturas y hago otra que recoje los IDs de los clientes de la tabla de los clientes y digo que me muestre el nombre de "Luis" y el apellido de "Rojas".

```sql
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId IN (
	SELECT TrackId
    FROM InvoiceLine
    WHERE InvoiceId IN (
		SELECT InvoiceId 
        FROM Invoice
        WHERE CustomerId IN (
			SELECT CustomerId 
            FROM Customer
            WHERE FirstName LIKE "Luis"
            AND LastName LIKE "Rojas"
        )
    )
);
```

# Query 7
En este ejercicio nos pide que mostremos las canciones que son más caras que cualquier otra canción del mismo álbum. Para esto, hago un `SELECT` del ID, del nombre de los álbumes y los IDs, nombre y precio unitario de las canciones,  y, luego realizo unas subconsulta sobre los IDs de los álbumes, que seleccione los IDs de las canciones y que iguales los IDs de estos.

```sql
SELECT A.AlbumId, A.Title, T.TrackId, T.Name ,T.UnitPrice
FROM album A, track T
WHERE A.AlbumId IN (
	SELECT T.TrackId
    FROM track T
    WHERE A.AlbumId = T.AlbumId
);
```

# Query 8
En este ejercicio nos pide que mostremos los clientes que han comprado todos los álbumes de Queen. Para esto, hago un `SELECT` de todo sobre la tabla de álbum y, luego realizo unas subconsulta sobre los IDs de los artistas, que seleccione los IDs de estos y que el nombre sea igual a "Queen".

```sql
SELECT * 
FROM Album
WHERE ArtistId = (
	SELECT ArtistId 
    FROM Artist
    WHERE name LIKE "Queen"
);
```

# Query 9
En este ejercicio nos pide que mostremos los clientes que han comprado más de 10 canciones en una sola transacción. Para esto, hago un `SELECT` de los IDs de los clientes y sus nombre y apellidos y, luego realizo unas subconsulta sobre los IDs de los clientes, que seleccione los IDs de las facturas y que el ID de las canciones esté más de 10 veces.

```sql
SELECT C.CustomerId, FirstName, LastName
FROM customer C
WHERE CustomerId IN (
	SELECT I.InvoiceId
    FROM invoiceline I
    WHERE TrackId > 10
);
```

# Query 10
En este ejercicio nos pide que mostremos las 10 canciones más compradas. Para esto, hago un `SELECT` de los IDs de las canciones y cuento la cantidad de estas y, luego realizo unas subconsulta sobre los IDs de las canciones, que seleccione estos IDs sobre la tabla de las líneas de las facturas y luego hago otra subconsulta que seleccione los IDs de las facturas sobre su tabla. Y por último, limito los datos a 10.

```sql
SELECT TrackId, COUNT(*)
FROM track
WHERE TrackId IN (
	SELECT TrackId
    FROM invoiceline
    WHERE InvoiceId IN (
		SELECT InvoiceId
        FROM invoice
    )
)
GROUP BY TrackId
LIMIT 10;
```

# Query 11
En este ejercicio nos pide que mostremos las canciones con una duración superior a la media. Para esto, hago un `SELECT` de los IDs de las canciones y de los milisegundos de estas y, luego realizo unas subconsulta sobre los milisegundos, que seleccione la media de estos. Y le pongo `>` para que me muestre los milisegundos mayores a la media de ellos.

```sql
SELECT TrackId, Milliseconds
FROM track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM track
);
```

# Query 12
En este ejercicio nos pide que mostremos el número de países donde tenemos clientes, el número de géneros músicales de los que disponemos y el número de pistas. Para esto, hago un `SELECT` de la cantidad de países sobre la tabla de clientes, hago una subconsulta para sacar la cantidad de géneros sobre la tabla de los géneros y hago otra subconsulta para mostrar el número de canciones que hay en total sobre la tabla de las canciones.

```sql
SELECT COUNT(DISTINCT Country) AS "Número países", 
	(SELECT COUNT(*)	
	FROM genre) AS "Número géneros",
		(SELECT COUNT(*)
		FROM track) AS "Número canciones"
FROM customer;
```

# Query 13
En este ejercicio nos pide que mostremos las canciones vendidas más que la media de ventas por canción. Para esto, hago un `SELECT` de los nombres de las canciones, de la suma de todas las canciones vendidad, y luego hago una operación para sacar el número de cuanto están por encima de la media. Luego hago varios `JOIN` para igualar los diversos IDs y agrupo por el nombre, el género y el precio unitario de las canciones. Y usando el `HAVING` digo que muestre la suma que sea mayor a la media.

```sql
SELECT T.Name AS "Nombre canción",
  SUM(IL.Quantity) AS "Unidades Vendidas",
  (SUM(IL.Quantity) / AVG(IL.Quantity)) AS "Cuanto por encima de la media"
FROM album A
JOIN track T 
ON A.AlbumId = T.AlbumId
JOIN genre G 
ON T.GenreId = G.GenreId
JOIN invoiceline IL 
ON T.TrackId = IL.TrackId
GROUP BY T.Name, T.GenreId, T.UnitPrice
HAVING SUM(IL.Quantity) > AVG(IL.Quantity);
```

# Query 14
En este ejercicio nos pide que clasifiquemos a los clientes en tres categorías según su país: “Local” si el país es ‘USA’, “Internacional” si el país es distinto de ‘USA’, y “Desconocido” si el país es nulo. Para esto, hago un `SELECT` de los IDs, de los nombres y apellidos y del país de los clientes. Luego, utilizando el `CASE`, digo que cuando el país sea "USA", lo cambie a "Local" y que cuando sea nulo, lo cambie a "Desconocido" y los demás que los cambie a "Internacional" y luego clasifico esa tabla como la categoria de los clientes.

```sql
SELECT C.CustomerId, C.FirstName, C.LastName, C.Country,
  CASE
    WHEN C.Country = 'USA' THEN 'Local'
    WHEN C.Country IS NULL THEN 'Desconocido'
    ELSE 'Internacional'
  END AS "Categotia del cliente"
FROM  customer C;
```

# Query 15
En este ejercicio nos pide que calculemos el descuento aplicado sobre cada factura, que depende del monto total de la factura (10% para facturas superiores a $10). Para esto, hago un `SELECT` de los IDs y del total de las facturas. Luego, utilizando el `CASE`, digo que cuando el total sea mayor que 10, lo multiplique por 0.1 y que sino, por 0 y lo clasifico como "Descuentos".

```sql
SELECT InvoiceId, Total,
  CASE
    WHEN Total > 10 THEN Total * 0.1 ELSE 0
  END AS Descuentos
FROM invoice;
```

# Query 16
En este ejercicio nos pide que calculemos Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado). Para esto, hago un `SELECT` de los IDs, de los nombres y apellidos y del título de los empleados. Luego, utilizando el `CASE`, digo que cuando el título contenga almenos la palabra "Manager", lo cambie por "Manager" y cuando contenga al menos "Assistant" lo cambie por "Asistente" y sino por "Empleado", clasificandolo como el nivel del empleado.

```sql
SELECT EmployeeId, FirstName, LastName, Title,
  CASE
    WHEN Title LIKE '%Manager%' THEN 'Manager'
    WHEN Title LIKE '%Assistant%' THEN 'Asistente'
    ELSE 'Empleado'
  END AS "Nivel de empleado"
FROM employee;
```

# Query 17
En este ejercicio nos pide que etiquetemos a los clientes como “VIP” si han gastado más de $45 en compras totales. Para esto, hago un `SELECT` de los IDs, de los nombres y apellidos de los clientes. Luego, utilizando el `CASE`, hago una subconsulta que calcula el importe total gastado por un cliente, y si da mayor de 45, ponga "VIP" y sino "Regular", clasificandolo como "Tipo de clinete".

```sql
SELECT C.CustomerId, C.FirstName, C.LastName,
  CASE
    WHEN (
      SELECT SUM(IL.UnitPrice * IL.Quantity)
      FROM invoiceline IL
      JOIN invoice I 
      ON IL.InvoiceId = I.InvoiceId
      WHERE I.CustomerId = C.CustomerId
    ) > 45 THEN 'VIP' ELSE 'Regular'
  END AS "Tipo de cliente"
FROM customer C;
```

# Query 18 - Extra
En este ejercicio nos pide que mpstremos las ventas totales de cada empleado. Para esto, hago un `SELECT` de los IDs, de los nombres y apellidos de los empleados. Luego, hago una subconsulta que calcula el importe total de las ventas de cada empleado, igualando los IDs de las facturas cuando los IDs de los clientes y de los empleados sean iguales; clasificandolo como "Total Sales".

```sql
SELECT E.EmployeeId, E.FirstName, E.LastName,
  (SELECT SUM(IL.UnitPrice * IL.Quantity)
    FROM invoiceline IL
    JOIN invoice I ON IL.InvoiceId = I.InvoiceId
    WHERE I.CustomerId = E.EmployeeId
  ) AS "Total Sales"
FROM employee E;
```