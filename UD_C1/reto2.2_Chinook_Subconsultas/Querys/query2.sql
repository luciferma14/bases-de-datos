-- Query 2
SELECT A.ArtistId, A.Name
FROM artist A
WHERE ArtistId IN (
	SELECT ArtistId
    FROM album
    WHERE AlbumId IN (
		SELECT AlbumId
        FROM track T
        WHERE Milliseconds IN (
			SELECT Milliseconds
            FROM track
            WHERE Milliseconds > 300000
        )
    )
);