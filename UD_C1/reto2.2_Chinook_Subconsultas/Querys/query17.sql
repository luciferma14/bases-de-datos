SELECT C.CustomerId, C.FirstName, C.LastName,
  CASE
    WHEN (
      SELECT SUM(IL.UnitPrice * IL.Quantity)
      FROM invoiceline IL
      JOIN invoice I 
      ON IL.InvoiceId = I.InvoiceId
      WHERE I.CustomerId = C.CustomerId
    ) > 45 THEN 'VIP' ELSE 'Regular'
  END AS "Tipo de cliente"
FROM customer C;