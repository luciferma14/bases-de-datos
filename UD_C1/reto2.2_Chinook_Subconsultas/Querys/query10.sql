SELECT TrackId, COUNT(*)
FROM track
WHERE TrackId IN (
	SELECT TrackId
    FROM invoiceline
    WHERE InvoiceId IN (
		SELECT InvoiceId
        FROM invoice
    )
)
GROUP BY TrackId
LIMIT 10;