-- Query 5
SELECT E.FirstName, (
	SELECT E.ReportsTo
    FROM employee E1
    JOIN employee E2
    ON E1.EmployeeId = E2.ReportsTo
    WHERE E.ReportsTo = E1.EmployeeId
)
FROM employee E
ORDER BY E.FirstName;