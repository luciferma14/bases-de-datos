SELECT DISTINCT(P.Name), T.Name
FROM playlist P, track T
WHERE P.Name LIKE 'M%'
AND EXISTS (
    SELECT 1
    FROM track T, playlisttrack PT
    JOIN Album A 
    ON T.AlbumId = A.AlbumId
    WHERE T.TrackId = PT.TrackId
    AND T.UnitPrice = (
        SELECT MIN(UnitPrice)
        FROM track T2
        JOIN Album A2 
        ON T2.AlbumId = A2.AlbumId
        WHERE T2.TrackId = PT.TrackId
        AND T2.AlbumId = A.AlbumId
        LIMIT 3
    )
    LIMIT 3
);