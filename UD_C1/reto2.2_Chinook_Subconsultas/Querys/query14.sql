SELECT C.CustomerId, C.FirstName, C.LastName, C.Country,
  CASE
    WHEN C.Country = 'USA' THEN 'Local'
    WHEN C.Country IS NULL THEN 'Desconocido'
    ELSE 'Internacional'
  END AS "Categotia del cliente"
FROM  customer C;