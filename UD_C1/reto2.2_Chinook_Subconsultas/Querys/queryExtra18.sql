SELECT E.EmployeeId, E.FirstName, E.LastName,
  (SELECT SUM(IL.UnitPrice * IL.Quantity)
    FROM invoiceline IL
    JOIN invoice I ON IL.InvoiceId = I.InvoiceId
    WHERE I.CustomerId = E.EmployeeId
  ) AS "Total Sales"
FROM employee E;