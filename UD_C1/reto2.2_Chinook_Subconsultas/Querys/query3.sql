-- Query 3
SELECT FirstName, LastName
FROM Employee
WHERE EmployeeId IN (
	SELECT DISTINCT SupportRepId
    FROM Customer
    WHERE SupportRepId IS NOT NULL
);

-- Con EXISTS
SELECT FirstName, LastName
FROM Employee
WHERE EXISTS (
	SELECT * -- true, 1, ... 
    FROM Customer
    WHERE SupportRepId = EmployeeId
);