-- Query 8
SELECT * 
FROM Album
WHERE ArtistId = (
	SELECT ArtistId 
    FROM Artist
    WHERE name LIKE "Queen"
);