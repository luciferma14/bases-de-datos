SELECT C.CustomerId, FirstName, LastName
FROM customer C
WHERE CustomerId IN (
	SELECT I.InvoiceId
    FROM invoiceline I
    WHERE TrackId > 10
);