SELECT T.Name AS "Nombre canción",
  SUM(IL.Quantity) AS "Unidades Vendidas",
  (SUM(IL.Quantity) / AVG(IL.Quantity)) AS "Cuanto por encima de la media"
FROM album A
JOIN track T 
ON A.AlbumId = T.AlbumId
JOIN genre G 
ON T.GenreId = G.GenreId
JOIN invoiceline IL 
ON T.TrackId = IL.TrackId
GROUP BY T.Name, T.GenreId, T.UnitPrice
HAVING SUM(IL.Quantity) > AVG(IL.Quantity);