SELECT TrackId, Milliseconds
FROM track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM track
);