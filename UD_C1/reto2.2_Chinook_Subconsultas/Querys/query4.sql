-- Query 4
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId NOT IN (
	SELECT TrackId
    FROM InvoiceLine
);