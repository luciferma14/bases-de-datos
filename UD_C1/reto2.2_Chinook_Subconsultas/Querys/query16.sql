SELECT EmployeeId, FirstName, LastName, Title,
  CASE
    WHEN Title LIKE '%Manager%' THEN 'Manager'
    WHEN Title LIKE '%Assistant%' THEN 'Asistente'
    ELSE 'Empleado'
  END AS "Nivel de empleado"
FROM employee;