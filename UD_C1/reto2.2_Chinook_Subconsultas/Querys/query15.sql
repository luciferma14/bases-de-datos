SELECT InvoiceId, Total,
  CASE
    WHEN Total > 10 THEN Total * 0.1 ELSE 0
  END AS Descuentos
FROM invoice;