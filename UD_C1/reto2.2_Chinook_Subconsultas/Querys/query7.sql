-- Query 7
SELECT A.AlbumId, A.Title, T.TrackId, T.Name ,T.UnitPrice
FROM album A, track T
WHERE A.AlbumId IN (
	SELECT T.TrackId
    FROM track T
    WHERE A.AlbumId = T.AlbumId
);