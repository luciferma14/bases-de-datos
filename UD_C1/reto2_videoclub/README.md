# Reto 2: Consultas básicas II
Lucía Ferrandis Martínez.

En este reto trabajamos con la base de datos `videoclub`, que nos viene dada en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto2_videoclub?ref_type=heads)

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/

## Query 1
En este primer ejercicio nos pide que mostremos los nombres y teléfonos de todos los clientes.
Para esto, realizo un `SELECT` del nombre (`Nom`) y le pongo el nombre "Nombre"; y también hago un `SELECT` del teléfono, que en la tabla es `Telefon`. Todos estos datos los saco de la tabla `CLIENT` haciendo un `FROM`. El código quedaría así:

```sql
SELECT 
	Nom AS Nombre,
    Telefon AS 'Teléfono'
FROM CLIENT
```

## Query 2
Este ejercicio nos pide que mostremos las fechas e importes de las facturas.
Esto lo realizamos haciendo un `SELECT` de la fecha (`DATA`) y el importe llamado `Import`. Y todos los datos los sacamos de la tabla `FACTURA` haciendo un `FROM`. El código quedaría así:

```sql
SELECT 
	Data AS Fecha,
    Import AS 'Importe'
FROM FACTURA
```

## Query 3
En este ejercicio nos pide que mostremos el la descripción (`Descripcio`) de los productos que hayan sido facturados en la factura número 3.
Para hacer esto, hacemos un `SELECT` de la descripción (`Descripcio`). Todo esto lo sacamos de la tabla `DETALLFACTURA` haciendo un `FROM`. Para conseguir los productos en la factura 3, lo que hacemos es un `WHERE` donde igualamos el código de la factura (`CodiFactura`) a 3. El código quedaría así:

```sql
SELECT 
	Descripcio AS Descripción
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Query 4
En este ejercicio nos pide que mostremos las facturas ordenadas de por descendiente por importe.
Para realizar esto, hago un `SELECT` todos los datos; añadiendo un *, y los sacamos de la tabla `FACTURA` haciendo un `FROM`. Y para sacar los datos de forma descendente, hago un `ORDER BY` para ordenar los datos de una consulta según las columnas, y cojo la columna `Import` ya que es la que me dide el ejercicio. Y a continuación, añado el `DESC` para indicar que quiero los datos de manera descendente. El código nos quedaría así:

```sql
SELECT *
FROM FACTURA
ORDER BY Import DESC;
```

## Query 5
En este ejercicio nos pide que mostremos los actores cuyo nombre empieze por "X".
Para realizar esto, lo que hacemos es un `SELECT` del nombre de los actores (`Nom`). Y todos estos datos los sacamos de la tabla `ACTOR` haciendo un `FROM`. Y para conseguir los datos que nos pide, hago un `WHERE` donde digo que el nombre tiene que ser igual a "X", y el % significa que también tienen que haber letras a partir de la "X". El código quedaría así:

```sql
SELECT 
	Nom AS "Nombre"
FROM ACTOR
WHERE Nom LIKE "X%";
```