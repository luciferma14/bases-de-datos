SELECT Title
FROM Album
WHERE A.AlbumId > (
	SELECT A2.AlbumId
    FROM Album A2
    JOIN Track T
    ON A2.AlbumId = T.AlbumId
)