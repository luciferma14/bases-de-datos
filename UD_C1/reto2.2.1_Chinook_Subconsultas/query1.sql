SELECT Name, Milliseconds
FROM Track T
WHERE T.Milliseconds > (
	SELECT AVG(T2.Milliseconds) 
    FROM Track T2
)
ORDER BY Milliseconds;