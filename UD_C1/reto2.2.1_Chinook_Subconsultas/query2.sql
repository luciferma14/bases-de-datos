SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
FROM Track T
GROUP BY T.AlbumId;

SELECT AVG(Num_Canciones)
FROM(
	SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
    FROM Track T
    GROUP BY T.AlbumId
) NTracks;

SELECT AlbumId, COUNT(*) AS n_tracks
FROM Track
GROUP BY AlbumId
HAVING n_tracks > (
	SELECT AVG(Num_Canciones)
    FROM (
		SELECT T.AlbumId, COUNT(T.TrackId) AS Num_Canciones
		FROM Track T
		GROUP BY T.AlbumId
	) NTracks
);