SELECT T.AlbumId, COUNT(T.TrackId) AS "Número canciones"
FROM Track T
WHERE T.AlbumId IN (
	SELECT A.AlbumId
    FROM Album A
    WHERE A.AlbumId = T.AlbumId
)
GROUP BY T.AlbumId;