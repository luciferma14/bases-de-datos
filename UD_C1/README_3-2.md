# Reto 3.2 Control de acceso en MySQL
Cuando hablamos de control de acceso en MySQL, nos referimos a DCL (Data Control Language), que se utiliza para controlar el acceso a los datos de la base de datos. Define los permisos que tienen los usuarios para realizar operaciones como seleccionar, insertar, actualizar y eliminar datos.

## Información
#### ¿Puede el usuario conectarse desde cualquier sitio?

MySQL nos permite especificar desde que host puede conectarse un usuario al crearlo. Para eso usuamos la función para crear usuarios:

```sql
CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'contraseña';
CREATE USER 'usuario'@'192.168.1.%' IDENTIFIED BY 'contraseña'; -- Desde una subred específica
CREATE USER 'usuario'@'%' IDENTIFIED BY 'contraseña'; -- Desde cualquier host
```
#### Cómo modificar y elimimar usuarios

Para modificarlos, tenemos dos opciones:

```sql
ALTER USER 'usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña'; -- Cambiar contraseña
```
Con ésta, cambias la contraseña.

```sql
RENAME USER 'prueba'@'localhost' TO 'prueba2'@'localhost'
```
Y ésta, cambias el nombre y/o el host.

Para eliminarlos, usamos:

```sql
DROP USER 'usuario'@'localhost';
```

#### Cómo se autentican los usuarios y opciones
* mysql_native_password:  El método predeterminado que usa un hash de la contraseña. Cabe decir que el complemento de autenticación está obsoleto y sujeto a eliminación en una versión futura de MySQL.
    ```sql
    CREATE USER 'usuario'@'localhost' IDENTIFIED WITH 'mysql_native_password' BY 'contraseña';
    ```
* caching_sha2_password: Proporciona mayor seguridad y es el predeterminado en versiones recientes.
    ```sql
    CREATE USER 'usuario'@'localhost' IDENTIFIED WITH 'caching_sha2_password' BY 'contraseña';
    ```

#### Cómo mostrar los usuarios existentes y sus permisos

Para mostrar a los usuarios, usamos el `SELECT` de esta forma:
```sql
SELECT user, host FROM mysql.user;
```
Haciendo este comando, también podemos ver los roles que hay creados, ya que están en la misma tabla, en la columna de los usuarios.

Y para ver los permisos de estos, hacemos:
```sql
SHOW GRANTS FOR 'usuario'@'localhost';
```

#### Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD

* Globales: `ALL PRIVILEGES`, `CREATE USER`, `GRANT OPTION`

* De Procedimiento: `EXECUTE`, `GRANT`

* De Base de Datos: `CREATE`, `DROP`, `ALTER`
    ```sql
    GRANT SELECT, INSERT ON database.* TO 'usuario'@'localhost'; -- Nivel de base de datos
    ```
* De Tabla: `SELECT`, `INSERT`, `UPDATE`, `DELETE`
    ```sql
    GRANT SELECT, INSERT, UPDATE, DELETE ON database.table TO 'usuario'@'localhost'; -- Nivel de tabla
    ```
* De Columna: Permisos específicos para columnas individuales.
    ```sql
    GRANT SELECT(columna1), UPDATE(columna2) ON database.table TO 'usuario'@'localhost'; -- Nivel de columna
    ```

#### Qué permisos hacen falta para gestionar a los usuarios y sus permisos

* `CREATE USER`: Para crear usuarios.
* `GRANT OPTION`: Para otorgar permisos a otros usuarios.
* `GRANT`: Para otorgar permisos a otros usuarios.
```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON *.* TO 'admin_usuario'@'localhost';
```

#### Posibilidad de agrupar los usuarios en grupos / roles

Los roles son conjuntos de permisos que pueden ser asignados a usuarios.

* Este es el proceso de crear el rol, darle los privigelios y darme permiso al un usuario para que lo pueda usar:

```sql
CREATE ROLE 'Jefe';
GRANT ALL PRIVILEGES ON *.* TO 'Jefe';
GRANT 'Jefe' TO 'usuario'@'localhost';
```

Para poner un rol a el usuario, usamos:
```sql
SET ROLE Jefe; 
```

#### Comandos que usamos para gestionar los usuarios y sus permisos

* Crear usuario: `CREATE USER`

* Modificar usuario: `ALTER USER`

* Eliminar usuario: `DROP USER`

* Mostrar usuarios: `SELECT user, host FROM mysql.user`

* Mostrar permisos de un rol: `SHOW GRANTS FOR <rol>`

* Asignar permisos: `GRANT`

* Revocar permisos: `REVOKE`

* Crear roles: `CREATE ROLE`

* Dar permiso de poner roles: `GRANT <role> TO <user>`

* Eliminar roles: `DROP ROLE`

(Resumen de varios comentados arriba)


## Referencias

* [www.hostinger.es](https://www.hostinger.es/tutoriales/como-crear-usuario-mysql)

* [dev.mysql.com](https://dev.mysql.com/doc/refman/8.3/en/access-control.html)