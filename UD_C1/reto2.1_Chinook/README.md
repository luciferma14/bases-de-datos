# Reto 2.1: Consultas con funciones de agregación
Lucía Ferrandis Martínez.

[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas
Para este reto, usaremos la base de datos Chinook, que podemos encontrar en el siguiente repositorio: https://github.com/lerocha/chinook-database/ . La base de datos Chinook simula una tienda de música en línea e incluye tablas para artistas, álbumes, canciones, clientes, empleados, géneros musicales, pedidos y facturas. Cada tabla contiene información relevante para su entidad respectiva, como nombres de artistas, títulos de álbumes,géneros musicales, detalles de pedidos y facturas, entre otros. Esta estructura permite practicar consultas SQL relacionadas con la gestión de una  tienda de música, desde la administración de inventario hasta el análisis de ventas y clientes.


* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto2.1_Chinook?ref_type=heads)


## Query 1
En este primer ejercicio nos pide que listemos todos los clientes de Francia. Para eso hago un `SELECT` de todo de la tabla `Customer`. Después a la tabla `Customer` le hago un `WHERE`donde digo que la columna `Country` sea igual que la palabra "Francia". El código quedaría así:

```sql
SELECT *
FROM Customer
WHERE Country LIKE "FRANCE";
```

## Query 2
En este ejercicio nos pide que listemos todas las facturas del primer trimestre de este año. Para eso hago un `SELECT` de todo de la tabla `Invoice`. Después a la tabla `Invoice` le hago un `WHERE`donde digo que la columna `InvoiceDate` este entre las fechas de enero de este año y marzo del mismo; usando el `BETWEEN` y la función de `DATE`. El código quedaría así:

```sql
SELECT *
FROM Invoice
WHERE InvoiceDate BETWEEN DATE("2024-01-01") AND DATE("2024-03-31");
```

## Query 3
En este ejercicio nos pide que listemos todas las canciones compuestas por AC/DC. Para eso hago un `SELECT` de todo de la tabla `Track`. Después a la tabla `Track` le hago un `WHERE`donde digo que la columna `Composer` sea igual que la palabra "AC/DC". El código quedaría así:

```sql
SELECT * 
FROM Track
WHERE Composer LIKE "AC/DC";
```

## Query 4
En este ejercicio nos pide que listemos las 10 canciones que más tamaño ocupan. Para eso hago un `SELECT` de todo de la tabla `Track`. Después la tabla `Track` la ordeno por los `Bytes` de manera descendente usando `ORDEN BY`. Y luego, limitandolo para que solo salgan las 10 primeras usando el `LIMIT 10`. El código quedaría así:

```sql
SELECT * 
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
En este ejercicio nos pide que listemos el nombre de aquellos países en los que tenemos clientes. Para eso hago un `SELECT DISTINCT` de los países la tabla `Customer`. Esto lo que hace es que solo muestra una vez cada nombre. El código quedaría así:

```sql
SELECT DISTINCT Country 
FROM Customer;
```

## Query 6
En este ejercicio nos pide que listemos todos los géneros musicales. Para eso hago un `SELECT` de todo la tabla `Genre` y lo ordeno sobre el nombre con el `ORDEN BY`. El código quedaría así:

```sql
SELECT * 
FROM Genre
ORDER BY Name
```

## Query 7
En este ejercicio nos pide que listemos todos los artistas junto a sus álbumes. Para eso hago un `SELECT` del nombre del artista, llamado "Artista" y también del título del álbum, llamado "Álbum". Después hago un `JOIN` de la tabla `Artist` con la tabla `Album`, y en el `ON` hago la igualdad de los IDs de los artistas con los álbumes. El código quedaría así: 

```sql
SELECT Ar.Name AS "Artista", Al.Title AS "Álbum"
FROM Artist Ar
JOIN Album Al
ON Ar.ArtistId = Al.ArtistId
```

## Query 8
En este ejercicio nos pide que mostremos los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen. Para esto hago un `SELECT` del nombre del empleado y del supervisor de la misma tabla. Para sacar los datos que nos pide hacemos un `JOIN` en la propia tabla `Employee` (haciendo referencia a ella dos veces como EM y EP). En el `ON` especificamos la condición para emparejar registros de empleados: `EM.ReportsTo = EP.EmployeeId`. Esto significa que el campo `ReportsTo` del empleado (a quién reporta) debe coincidir con el `EmployeeId` del supervisor. Con el `ORDER BY` ordenamos de manera descendente por la fecha de cumpleaños de los empleados, para que nos salgan los más jóvenes. Y limitamos a 5 los datos. El código quedaría así:

```sql
SELECT 
	EM.FirstName AS "Nombre Empleado",
	EP.FirstName AS "Nombre Supervisor"
FROM Employee EM
JOIN Employee EP
ON EM.ReportsTo = EP.EmployeeId
ORDER BY EM.BirthDate DESC
LIMIT 5;
```

## Query 9
Para este ejercicio nos pide que mostremos todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden). Para esto hacemos un `SELECT` de todos los datos en ese mismo orden y añadiendoles el alias correspodiente. Las tablas que vamos a utilizar son la de las facturas y la de los clientes. Y en el `WHERE` igualamos los países de los clientes ha Alemania y la ciudad de las facturas a Berlín. El código quedaría así:

```sql
SELECT 
	I.InvoiceDate "Factura",
    C.FirstName "Nombre",
    C.LastName "Apellido",
    I.BillingAddress "Dirección de facturación",
    I.BillingPostalCode "Código postal",
    I.BillingCountry "País",
    I.Total "Importe"
FROM 
	Invoice AS I,
    Customer AS C
WHERE C.Country LIKE "Germany"
AND I.BillingCity like "Berlin";
```

## Query 10
En este ejercicio nos pide que mostremos las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración. Para esto hacemos un `SELECT` del nombre de la playlist, el de la canción, el álbum y el tiempo de cada canción. A la tabla `Playlist P` la hacemos un `JOIN` con la tabla `PlaylistTrack PT` e igualamos los IDs de las tablas. Luego hacemos otro `JOIN` de `PlaylistTrack PT` con `Track T` e igualamos los IDs, continuando, hacemos otro `JOIN` de `Track T` con `Album A` e igualamos los IDs. En el `WHERE` decimos que el nombre de la playlist tiene que empezar por "C" y lo ordenamos por el nombre de la playlist, el título del álbum y por el tiempo de las canciones, de manera descendente. El código quedaría así:
```sql
SELECT P.Name AS "PlayList", T.Name AS "Canción", A.Title AS "Álbum", T.Milliseconds AS "Tiempo (m)"
FROM Playlist P
JOIN PlaylistTrack PT 
ON P.playlistId = PT.playlistId
JOIN Track T 
ON PT.TrackId = T.trackId
JOIN Album A 
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'C%'
ORDER BY P.Name, A.Title, T.Milliseconds DESC;
```

##  Query 11
En este ejercicio nos pide que mostremos qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido. Para esto hacemos un `SELECT` del nombre y apellido de los cliente, de la tabla `Customer C` y le hacemos un `JOIN` con la tabla `Invoice` que es la de las facturas, donde igualamos los IDs de dichas tablas. Luego decimos que el importe total de las facturas tiene que ser mayor que 10. Y lo ordenamos por el apellido de los clientes de manera descendente. El código quedaría así:

```sql
SELECT FirstName AS "Nombre", LastName AS "Apellido"
FROM Customer C
JOIN Invoice I
ON C.CustomerId = I.CustomerId
WHERE I.Total > '10'
ORDER BY C.LastName DESC;
```

##  Query 12
En este ejercicio nos pide que mostremos el importe medio, mínimo y máximo todas las factura. Para esto hacemos un `SELECT` de la media aplicando la función de `AVG()` sobre la columna `Total`, y nos calcula a media de los datos que haya en dicha columna. Hacemos lo mismo pero aplicando la función de `MAX()` y `MIN()` para sacar el máximo y mínimo respectivamente. Todo esto lo sacamos de la tabla de las facturas `Invoice`. El código quedaría así:

```sql
SELECT 
	AVG(Total) AS "Media del importe de las facturas",
    MAX(Total) AS "Máximo importe",
    MIN(Total) AS "Mínimo importe"
FROM Invoice;
```

##  Query 13
En este ejercicio nos pide que mostremos el número total de artistas. Para esto hacemos un `SELECT` con la función que nos permite contar los datos `COUNT(Name)` y le decimos que cuente los nombres de los artitas, sobre dicha tabla. El código quedaría así:

```sql
SELECT COUNT(Name) AS "Número total de artistas"
FROM Artist;
```

##  Query 14
En este ejercicio nos pide que mostremos el número de canciones del álbum “Out Of Time”. Para esto hacemos un `SELECT` con la función que nos permite contar los datos `COUNT(Name)` llamado "Número canciones" y le decimos que cuente los nombres de las canciones, sobre dicha tabla. Luego hacemos un `JOIN` con la tabla `Album A` e igualamos los IDs de los álbumnes. En el `WHERE` decimos que el título del álbum tiene que ser igual a "Out of time". El código quedaría así:

```sql
SELECT COUNT(T.Name) AS "Número canciones"
FROM Track T
JOIN Album A
ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE "Out of time";
```

##  Query 15
En este ejercicio nos pide que mostremos el número países donde tenemos clientes. Para esto hacemos un `SELECT` con la función que nos permite contar los datos `COUNT()` y le añadimos el `DISTINCT` para que cuente solo las que no están repetidas, llamado "Países" y le decimos que cuente los nombres de los países, sobre la tabla de los clientes. En el `WHERE` decimos que los países tiene que ser distinto de `NULL` o mayor/igual que 0, para que nos salgan los que tiene clientes. El código quedaría así:

```sql
SELECT COUNT(DISTINCT Country) AS "Países"
FROM Customer
WHERE Country != " " OR Country >= 0;
```

##  Query 16
En este ejercicio nos pide que mostremos el número de canciones de cada género (deberá mostrarse el nombre del género). Para esto hacemos un `SELECT` del nombre de los géneros y con la función que nos permite contar los datos `COUNT(G.GenreId)` llamado "Número de canciones" y le decimos que cuente los IDs de los géneros, sobre la tabla de las canciones. Luego hacemos un `JOIN` con la tabla `Genre G` e igualamos los IDs de los géneros. Después agrupamos sobre los IDs de los géneros. El código quedaría así:

```sql
SELECT G.Name AS "Género", COUNT(G.GenreId) AS "Número de canciones"
FROM Track T
JOIN Genre G
ON T.GenreId = G.GenreId
GROUP BY G.GenreId;
```

##  Query 17
En este ejercicio nos pide que mostremos los álbumes ordenados por el número de canciones que tiene cada uno. Para esto hacemos un `SELECT` de los IDs de los álbumes y con la función que nos permite contar los datos `COUNT(T.TrackId)` llamado "Número canciones" y le decimos que cuente los IDs de las canciones, sobre la tabla de los álbumes. Luego hacemos un `JOIN` con la tabla `Track T` e igualamos los IDs de los álbumes. Después agrupamos sobre los IDs de los álbumes. Y ordenamos de manera que cuente los IDs de los álbumes de las canciones. El código quedaría así:

```sql
SELECT A.AlbumId AS "Álbumes", COUNT(T.TrackId) AS "Número canciones"
FROM Album A
JOIN Track T
ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
ORDER BY COUNT(T.AlbumId);
```

##  Query 18
En este ejercicio nos pide que encontremos los géneros musicales más populares (los más comprados). Para esto hacemos un `SELECT` de los nombres de los géneros y con la función que nos permite contar los datos `COUNT(T.TrackId)` llamado "Número canciones" y le decimos que cuente los IDs de las canciones, sobre la tabla de los géneros. Luego hacemos un `JOIN` con la tabla `Track T` e igualamos los IDs de los géneros. A continuación, hacemos otro `JOIN` con la tabla `InvoiceLine IL` e igualamos los IDs de las canciones. Después agrupamos sobre los IDs de los géneros. El código quedaría así:

```sql
SELECT G.Name AS "Género", COUNT(T.TrackId) AS "Número canciones"
FROM Genre G
JOIN Track T
ON G.GenreId = T.GenreId
JOIN InvoiceLine IL
ON T.TrackId = IL.TrackId
GROUP BY G.GenreId;
```

##  Query 19
En este ejercicio nos pide que listemos los 6 álbumes que acumulan más compras. Para esto hacemos un `SELECT` de los títulos de los álbumes. Luego, en el `WHERE` igualamos los IDs de los álbumes de dicha tabla con la de las canciones. Después agrupamos sobre los títulos de los álbumes, ordenamos todo de manera desdendente. Y limitamos a 6 resultados. El código quedaría así:

```sql
SELECT A.Title AS "Título"
FROM Album A, Track T
WHERE A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY COUNT(*) DESC
LIMIT 6;
```

##  Query 20
En este ejercicio nos pide que mostremos los países en los que tenemos al menos 5 clientes. Para esto hacemos un `SELECT` de los países y con la función que nos permite contar los datos `COUNT(*)` llamado "Número clientes" y le decimos que cuente todo de la tabla `Customer C`. Después agrupamos sobre los países, y con el `HAVING` decimos que cuente los países que tiene como mínimo 5 clientes. El código quedaría así:

```sql
SELECT Country, COUNT(*) AS "Número clientes"
FROM Customer C
GROUP BY Country
HAVING COUNT(C.Country) >= 5;
```