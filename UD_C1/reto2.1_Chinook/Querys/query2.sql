SELECT *
FROM Invoice
WHERE InvoiceDate BETWEEN DATE("2024-01-01") AND DATE("2024-03-31");

--Otra forma--

SELECT *
FROM Invoice
WHERE QUARTER (InvoiceDate) = 1  
AND YEAR(InvoiceDate) = YEAR(CURRENT_DATE());