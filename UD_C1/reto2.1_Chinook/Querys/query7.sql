SELECT Ar.Name AS "Artista", Al.Title AS "Album"
FROM Artist Ar
JOIN Album Al
ON Ar.ArtistId = Al.ArtistId