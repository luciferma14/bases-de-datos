SELECT Country, COUNT(*) AS "Número clientes"
FROM Customer C
GROUP BY Country
HAVING COUNT(C.Country) >= 5;