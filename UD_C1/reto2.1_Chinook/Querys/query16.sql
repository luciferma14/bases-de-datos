SELECT G.Name AS "Género", COUNT(G.GenreId) AS "Número de canciones"
FROM Track T
JOIN Genre G
ON T.GenreId = G.GenreId
GROUP BY G.GenreId;