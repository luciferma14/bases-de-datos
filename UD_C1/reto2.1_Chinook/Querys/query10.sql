SELECT P.Name AS "PlayList", T.Name AS "Canción", A.Title AS "Álbum", T.Milliseconds AS "Tiempo (m)"
FROM Playlist P
JOIN PlaylistTrack PT 
ON P.playlistId = PT.playlistId
JOIN Track T 
ON PT.TrackId = T.trackId
JOIN Album A 
ON T.AlbumId = A.AlbumId
WHERE P.Name LIKE 'C%'
ORDER BY P.Name, A.Title, T.Milliseconds DESC;