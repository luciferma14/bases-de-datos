SELECT 
	I.InvoiceDate "Factura",
    C.FirstName "Nombre",
    C.LastName "Apellido",
    I.BillingAddress "Dirección de facturación",
    I.BillingPostalCode "Código postal",
    I.BillingCountry "País",
    I.Total "Importe"
FROM 
	Invoice AS I,
    Customer AS C
WHERE C.Country LIKE "Germany"
AND I.BillingCity like "Berlin";