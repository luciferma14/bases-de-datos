SELECT 
	AVG(Total) AS "Media del importe de las facturas",
    MAX(Total) AS "Máximo importe",
    MIN(Total) AS "Mínimo importe"
FROM Invoice;