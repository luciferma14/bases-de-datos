SELECT COUNT(T.Name) AS "Número canciones"
FROM Track T
JOIN Album A
ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE "Out of time";