SELECT G.Name AS "Género", COUNT(T.TrackId) AS "Número canciones"
FROM Genre G
JOIN Track T
ON G.GenreId = T.GenreId
JOIN InvoiceLine IL
ON T.TrackId = IL.TrackId
GROUP BY G.GenreId;