SELECT A.AlbumId, A.Title, AR.Name, COUNT(*)
FROM InvoiceLine IL
JOIN Track T
ON T.TrackId = IL.TrackId
JOIN Album A
ON A.AlbumId = T.AlbumId
JOIN Artist AR
ON A.ArtistId = AR.ArtistId
GROUP BY A.AlbumId
ORDER BY COUNT(*) DESC;