SELECT 
	EM.FirstName AS "Nombre Empleado",
	EP.FirstName AS "Nombre Supervisor"
FROM Employee EM
JOIN Employee EP
ON EM.ReportsTo = EP.EmployeeId
ORDER BY EM.BirthDate DESC
LIMIT 5;