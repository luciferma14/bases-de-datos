SELECT A.Title AS "Álbumes", COUNT(T.TrackId) AS "Número canciones"
FROM Album A
JOIN Track T
ON A.AlbumId = T.AlbumId
GROUP BY A.AlbumId
ORDER BY COUNT(T.AlbumId) DESC;