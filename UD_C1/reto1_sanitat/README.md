# Reto 1: Consultas básicas
Lucía Ferrandis Martínez.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads)

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD` que al añadir `AS`, podemos cambiar el nombre de esta columna, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS "Código",
NOM,
TELEFON
FROM HOSPITAL;
```

## Query 2

Este ejercicio es muy parecido al anterior, lo que hay diferente es que, tenemos que mostrar los hospitales existentes que tengan una letra "A" en la segunda posición del nombre. Para poder hacer esto, lo que hago es un substring para así, a partir del nombre, seleccione la "a" en la segunda posición. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS "Código",
NOM,
TELEFON
FROM HOSPITAL
WHERE SUBSTRING(NOM, 2, 1) = 'A';
```

## Query 3

Para seleccionar el código del hopital (`HOSPITAL_COD`), el código de sala (`SALA_COD`), el número del empleado (`EMPLEAT_NO`) y el apellido (`COGNOM`), lo que hago es un `SELECT` de todos estos datos sobre la tabla `PLANTILLA`. El código se verÍa así:

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```

## Query 4

Este ejercicio es muy parecido al anterior, por lo que, hago lo mismo para conseguir los datos del principio; lo único diferente es que, en este ejercicio nos pedian que mostraramos a todos los trabajadores exepto los que tenian turno de noche. Para hacer esto, lo que hago es un `WHERE` que diga que muestre los turnos (`TORN`), menos los que contengan la letra "N", que es así como esta representado el turno de noche. El código se verÍa así:

```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA
WHERE TORN != 'N';
```

## Query 5

Para este ejercicio nos pedian mostrar a los enfermos que hayan nacido en el año 1960. Para esto, hago un `SELECT` de el dato que nos muestra la fecha de nacimiento (`DATA_NAIX`), de la tabla `MALALT`. Y para mostrar lo que nos pedia, hago un `WHERE` usando la opción de `YEAR` y digo que muestre solo los datos que haya en `DATA_NAIX` que sea igual a 1960. El código se verÍa así:

```sql
SELECT DATA_NAIX
FROM MALALT
WHERE YEAR (DATA_NAIX)  = '1960';
```

## Query 6

Este ejercicio es igual que el anterior, lo que cambia es que nos pide que muestre a los enfermos nacidos a partir de 1960. Que es añadir el > al igual, para que sea mayor o igual a 1960. El código se verÍa así:

```sql
SELECT DATA_NAIX
FROM MALALT
WHERE YEAR (DATA_NAIX)  >= '1960';
```