SELECT 
	HOSPITAL_COD AS "Código",
	NOM,
	TELEFON
FROM HOSPITAL
WHERE SUBSTRING(NOM, 2, 1) = 'A';
# ( ) -> tabla, número de posiciones, número de letras que quieres buscar

# Otra forma

SELECT 
	HOSPITAL_COD AS "Código",
	NOM,
	TELEFON
FROM HOSPITAL
WHERE NOM LIKE "_a%"

# _ por cada posición, % todos los caracteres restantes