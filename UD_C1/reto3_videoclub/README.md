# Reto 3: Consultas básicas con JOIN
Lucía Ferrandis Martínez.

En este reto trabajamos con la base de datos `videoclub`, que nos viene dada en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace repositorio](https://gitlab.com/luciferma14/bases-de-datos.git)

* [Enlace ejercicio](https://gitlab.com/luciferma14/bases-de-datos/-/tree/main/UD_C1/reto3_videoclub?ref_type=heads)

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/


## Query 1
En este primer ejercicio nos pide que listemos todas las películas del videoclub junto al nombre de su género. Para eso hago un `SELECT` del titulo de la tabla `PELICULA` y la descripción de la tabla `GENERE`. Después a la tabla `PELICULA` de hago un `JOIN` con la tabla `GENERE`, para poder mezclar las dos tablas. Y digo en el `ON` que el código del género (`CodiGenere`) de la tabla `PELICULA` sea igual que el de la tabla `GENERE`. El código quedaría así:

```sql
SELECT 
	P.Titol,
	G.Descripcio
FROM PELICULA P
JOIN GENERE G
ON P.CodiGenere = G.CodiGenere;
```

## Query 2
En este ejercicio nos pide que listemos todas las facturas de María. Para eso hago un `SELECT` de todos los datos de la tabla `CLIENT` (`C.*`), y de la tabla `FACTURA` cojo el código de la factura, la fecha, y el importe. Después le hago un `JOIN` a la tabla `CLIENT` con la tabla  `FACTURA` y digo que el DNI de la tabla `FACTURA` sea igual que el de la tabla `CLIENT`. Y en `WHERE` digo que el nombre de la tabla `CLIENT` tiene que ser igual la palabra "Maria". El código quedaría así:

```sql
SELECT C.*, F.CodiFactura, F.Data, F.Import
FROM CLIENT C
JOIN FACTURA F
ON F.DNI = C.DNI
WHERE C.Nom LIKE "%Maria%";
```

## Query 3
En este ejercicio nos pide que listemos las películas junto a su actor principal. Para hacer esto hago un `SELECT` del nombre de la tabla `ACTOR` y el título de la tabla `PELICULA`. Luego hago un `JOIN` de la tabla `PELICULA` con la tabla `ACTOR` e igualo los códigos del actor respectivamente. El código quedaría así:

```sql
SELECT 
	A.Nom,
	P.Titol
FROM PELICULA P
JOIN ACTOR A
ON P.CodiActor = A.CodiActor;
```

## Query 4
Este ejercicio es muy parecido al anterior, lo que nos pide que listemos las películas junto a todos los actores que la interpretaron. Para hacer esto hago un `SELECT` del nombre de la tabla `INTERPRETADA` y el título de la tabla `PELICULA`. Luego hago un `JOIN` de la tabla `PELICULA` con la tabla `INTERPRETADA` y tambien lo hago con la tabla `ACTOR` e igualo el código de la película de la tabla `PELICULA` con el de la tabla `INTERPRETADA` y igualo también el código del actor de la tabla `INTERPRETADA` con el de la tabla `ACTOR`. El código quedaría así:

```sql
SELECT 
	A.Nom,
	P.Titol
FROM PELICULA P
JOIN INTERPRETADA I
JOIN ACTOR A
ON P.CodiPeli = I.CodiPeli
AND I.CodiActor = A.CodiActor;
```

## Query 5
Este ejercicio nos pide que listemos los ID y nombres de las películas junto a los ID y nombres de sus segundas partes. Para esto hago un `SELECT` del título de la tabla `PELICULA` y también el código de la película, estos datos los cojo dos veces, ya que en la tabla `PELICULA` también se encuetran los datos de las segundas partes. Entonces hago un `JOIN` de la tablas diciendo que el código de la película sea igual que la sengunda parte. El código quedaría así:

```sql
SELECT 
	P.Titol AS Película,
	P.CodiPeli AS Código,
    PL.Titol AS "Segunda parte",
	PL.CodiPeli AS "Código segunda"
FROM PELICULA P
JOIN PELICULA PL
ON P.CodiPeli = PL.SegonaPart;
```