SELECT 
	A.Nom,
	P.Titol
FROM PELICULA P
JOIN INTERPRETADA I
JOIN ACTOR A
ON P.CodiPeli = I.CodiPeli
AND I.CodiActor = A.CodiActor;
-- Películas + todos los actores