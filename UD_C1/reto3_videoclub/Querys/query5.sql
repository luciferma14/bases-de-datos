SELECT 
	P.Titol AS Película,
	P.CodiPeli AS Código,
    PL.Titol AS "Segunda parte",
	PL.CodiPeli AS "Código segunda"
FROM PELICULA P
JOIN PELICULA PL
ON P.CodiPeli = PL.SegonaPart;