SELECT 
	P.Titol,
	G.Descripcio
FROM PELICULA P -- A la tabla PELICULA le hago un JOIN con la tabla GENERE
JOIN GENERE G
ON P.CodiGenere = G.CodiGenere;