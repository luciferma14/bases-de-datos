# Reto 3.1 Definición de datos en MySQL
Cuando hablamos de definición de datos, nos referimos a DDL (Data Definition Language), que se utiliza para definir la estructura de la base de datos, incluyendo la creación, modificación y eliminación de objetos como tablas, vistas y usuarios.

A lo largo de la explicación, también veremos unos ejemplos de TCL (Transaction Control Language), que se utiliza para controlar las transacciones en una base de datos y garantizan la integridad de los datos al permitir confirmar o deshacer los cambios realizados dentro de una transacción.

## Información
Nos encontramos con una base de datos para una aplicación de gestión de vuelos. 
Para conectarnos al servidor de ésta, usamos `mysql` de esta forma, usando nuestro usuario y contreseña:
```sql
mysql -u root -p -h 127.0.0.1 -P 33006 -- (luego te pide la contraseña)
```
Para la creación de la base de datos, usaremos el comando `CREATE SCHEMA`:

```sql
CREATE SCHEMA `Rallané`;
```

Ya que tenemos la base de datos, siguiente que haremos será, crear las tablas. Ésta necesita tres: Vuelos, Pasajeros y Reservas., añadiendo las columnas necesarias:

Creamos la tabla de los pasajeros:
```sql
CREATE TABLE `Rallané`.`pasajeros` (
  `idPasajeros` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(45) NOT NULL,
  `tipoDocuento` VARCHAR(45) NOT NULL,
  `fechaNacimiento` DATE NOT NULL,
  PRIMARY KEY (`idPasajeros`));
```
Creamos la tabla de los vuelos:
```sql
CREATE TABLE `rallané`.`vuelos` (
  `idVuelos` INT NOT NULL,
  `origen` VARCHAR(45) NOT NULL,
  `destino` VARCHAR(45) NOT NULL,
  `asientos` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idVuelos`));
```
Creamos la tabla de las reservas:
```sql
CREATE TABLE `rallané`.`reservas` (
  `idReservas` INT NOT NULL,
  `idPasajero` INT NOT NULL,
  `idVuelo` INT NOT NULL,
  `fecha` DATE NOT NULL,
  PRIMARY KEY (`idReservas`),
  INDEX `idPasajero_idx` (`idPasajero` ASC) VISIBLE,
  INDEX `idVuelo_idx` (`idVuelo` ASC) VISIBLE,
  CONSTRAINT `idPasajero`
    FOREIGN KEY (`idPasajero`)
    REFERENCES `rallané`.`pasajeros` (`idPasajeros`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idVuelo`
    FOREIGN KEY (`idVuelo`)
    REFERENCES `rallané`.`vuelos` (`idVuelos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
```
Aquí lo que hacemos es referenciar las claves foráneas de las dos tablas anteriores.

## Explicación tablas 
Se utilizan tres tablas para separar la información de los vuelos, los pasajeros y las reservas, mejorando la organización y evitando la repetición de datos. 

Cada tabla tiene una clave primaria única para identificar cada registro. Se utilizan claves foráneas para relacionar las tablas entre sí. Por ejemplo, vuelos.idVuelo hace referencia en reservas.idVuelo, esto asegura que solo se puedan realizar  reservas para vuelos existentes. También tenemos pasajeros.idPasajero que hace referencia en reservas.idPasajero, que nos garantiza que  las reservas solo se hagan para pasajeros registrados. 


En cuanto a las restricciones, ayudan a mantener la seguridad de los datos y evitar errores. Impide que se asignen asientos a dos pasajeros diferentes en el mismo vuelo, evitar que se realicen reservas para vuelos inexistentes y garantizar que solo se registren pasajeros con información completa.


Estas restricciones tienen ventajas e inconvenientes. Sobre las ventajas, podemos decir que tiene mayor confiabilidad de los datos, la reducción de los errores y las ambigüedades es más grande y también ayuda a la simplificación de las consultas y operaciones en la base de datos.

En cuanto a los inconvenientes, podemos decir que tiene una mayor complejidad en la definición y gestión de la estructura de datos y puede llegar a existir una posible restricción de la flexibilidad del sistema.


## Explicación comandos 
Continuando con la información, tenemos varios comandos que nos permiten navegar por la base de datos y poder presenciar toda la información de dicha base.

Para navegar por la base de datos, necesitamos algunos comandos:

* Para explorar la base de datos, utilizaremos: 
```sql
SHOW DATABASES; 
SHOW SCHEMAS; -- También tenemos esta forma
```
* * Muestra una lista con las bases de datos disponibles en el servidor. 
#
* Para saber la base de datos, usamos: 
```sql
USE Chinook; 
```
* * Selecciona la base de datos con la que quieres trabajar.
```sql
SELECT DATABASE();
```
* * Muestra en qué tabla estás trabajando
#
* Para explorar las tablas de una base de datos, usamos: 
```sql
SHOW TABLES; 
```
* * Muestra una lista con las tablas existentes en la base de datos seleccionada.
#
* Para explorar los campos de una tabla y conocer los tipos de datos y las restricciones, usamos: 
```sql
DESCRIBE Chinook.Artist; 
SHOW COLUMNS FROM Chinook.Artist; -- También tenemos esta forma
```
* * Muestra información detallada sobre la estructura de la tabla, incluyendo los nombres de los campos, los tipos de datos, las restricciones y las claves..
#
* Para gestionar las transacciones, usamos:
```sql
COMMIT; 
```
* * Para confirmar los cambios realizados.
#
```sql
ROLLBACK; 
```
* * Para deshacer los cambios desde el último COMMIT. Una vez que haces COMMIT, ya no puedes volver atrás. Si no haces el COMMIT, si que borra todo lo que has hecho.
#
```sql
SELECT @@autocommit;
```
* * Para ver el estado del autocommit:
```sql
SET @@autocommit=1; -- Para activarlo
SET @@autocommit=0; -- Para desactivarlo
```
* * Para activar o desactivar la ejecución automática de COMMT al finalizar cada sentencia SQL: